CC:=gcc
CFLAGS:=-Wall -Wextra -std=c99 -pedantic
LIBS=

all: lampo

lampo: main.c
	$(CC) $(CFLAGS) $(LIBS) $< -o $@
